#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
    数据库表格定义
'''


def getTableCrtSqls():
    sqls=list()
    sqls.append('CREATE TABLE fuelUpdateHistorys(_id INTEGER PRIMARY KEY AUTOINCREMENT,startDate TEXT ,description TEXT)')
    sqls.append('CREATE TABLE FuelClasses(_id INTEGER PRIMARY KEY AUTOINCREMENT,code INTEGER NOT NULL,name TEXT NOT NULL,grades TEXT)')
    sqls.append('CREATE TABLE Fuels(_id INTEGER PRIMARY KEY AUTOINCREMENT,fuelClass INTEGER NOT NULL,fuelName TEXT NOT NULL,gradeLevel INTEGER NOT NULL,gradeName TEXT NOT NULL, updateHistory TEXT, isTrace INTEGER)')
    sqls.append('CREATE TABLE FuelPrices(_id INTEGER PRIMARY KEY AUTOINCREMENT,fuel INTEGER, date INTEGER,price REAL)')
    sqls.append('CREATE TABLE GpsTypes(_id INTEGER PRIMARY KEY AUTOINCREMENT,typecode INTEGER,typename TEXT NOT NULL, desc TEXT)')
    sqls.append('CREATE TABLE FuelStations(_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,address TEXT,latitude REAL,longitude REAL,coordinateType INTEGER NOT NULL,weight INTEGER)')
    sqls.append('CREATE TABLE VehicleInfos(_id INTEGER PRIMARY KEY AUTOINCREMENT,identificationNumber TEXT NOT NULL,totalScale INTEGER,boxVolume INTEGER NOT NULL,useFuel INTEGER NOT NULL,maintenanceMileage INTEGER NOT NULL,maintenanceMonth INTEGER NOT NULL)')
    sqls.append('CREATE TABLE ToFuelRecords(_id INTEGER PRIMARY KEY AUTOINCREMENT,vehicle INTEGER NOT NULL,date INTEGER NOT NULL,fuel INTEGER NOT NULL,mileage INTEGER NOT NULL,fuelDial REAL NOT NULL,money REAL NOT NULL,fuelAmount REAL NOT NULL,price REAL NOT NULL,station INTEGER)')
    sqls.append('create table jobClasses(_id INTEGER PRIMARY KEY AUTOINCREMENT,jcName TEXT)')
    sqls.append('create table repairer(_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,address TEXT)')
    sqls.append('create table maintences(_id INTEGER PRIMARY KEY AUTOINCREMENT,date INTEGER,mileage INTEGER,repairer INTEGER, jobClass INTEGER, totalCost REAL, materialCost REAL, jobContents TEXT)')
    return sqls

#维护记录表
TN_MR = 'maintences'
FN_M_DATE='date'
FN_M_MILEAGE = 'mileage'
FN_M_REPAIRER = 'repairer'
FN_M_JC = 'jobClass'
FN_M_TC = 'totalCost'
FN_M_MC = 'materialCost'
FN_M_JC = 'jobContents'
TC_M_DATE,TC_M_MILEGAE,TC_M_REPAIRER,TC_M_JOBCLS,TC_M_TCOST,TC_M_MCOST,TC_M_CONTENTS = range(1,8)

#燃料表索引
TN_FUELS = 'Fuels'
FN_F_FC = 'fuelClass'
FN_F_NAME = 'fuelName'
FN_F_GL = 'gradeLevel'
FN_F_GNAME = 'gradeName'
FN_F_UH = 'updateHistory'
FN_F_ISTRACE = 'isTrace'
TC_F_FC,TC_F_NAME,TC_F_GL,TC_F_GNAME,TC_F_UH,TC_F_ISTRACE = range(1,7)

#加油记录表索引
TN_TFR = 'ToFuelRecords'
FN_TF_VEHICLE = 'vehicle'
FN_TF_DATE = 'date'
FN_TF_FUEL = 'fuel'
FN_TF_MILEAGE = 'mileage'
FN_TF_FUELDIAL = 'fuelDial'
FN_TF_MONEY = 'money'
FN_TF_AMOUNT = 'fuelAmount'
FN_TF_PRICE = 'price'
FN_TF_STATION = 'station'
TC_TF_VEHICLE,TC_TF_DATE,TC_TF_FUEL,TC_TF_MILEAGE,TC_TF_FUELDIAL,TC_TF_MONEY,TC_TF_AMOUNT,TC_TF_PRICE,TC_TF_STATION = range(1,10)

#车辆信息表
TN_VINFO = 'VehicleInfos'
FN_VI_LICENSE = 'identificationNumber' # , ,useFuel , ,
FN_VI_TOTAL = 'totalScale'
FN_VI_BOXVOLUME = 'boxVolume'
FN_VI_FUEL = 'useFuel'
FN_VI_MILEAGE = 'maintenanceMileage'
FN_VI_MONTH = 'maintenanceMonth'
TC_VI_LICENSE,TC_VI_TOTAL,TC_VI_VOLUME,TC_VI_FUEL,TC_VI_MILEAGE,TC_VI_MONTH = range(1,7)

#加油站表
TN_STATION = 'FuelStations'
FN_S_NAME = 'name'
FN_S_ADDRESS = 'address'
FN_S_LAT = 'latitude'
FN_S_LON = 'longitude'
FN_S_CT = 'coordinateType'
FN_S_WEIGHT = 'weight'
TC_S_NAME,TC_S_ADDR,TC_S_LAT,TC_S_LON,TC_S_CT,TC_S_WEIGHT = range(1,7)

#修理厂表
TN_REPAIRER = 'repairer'

#作业类别表
TN_JC = 'jobClasses'

#燃料类别表
TN_FC = 'FuelClasses'
FN_FC_CODE = 'code'
FN_FC_NAME = 'name'
FN_FC_GRADES = 'grades'
TC_FC_CODE,TC_FC_NAME,TC_FC_GRADES = range(1,4)

#燃料价格表
TN_FP = 'FuelPrices'
FN_FP_FUEL = 'fuel'
FN_FP_DATE = 'date'
FN_FP_PRICE = 'price'
TC_FP_FUEL,TC_FP_DATE,TC_FP_PRICE = range(1,4)