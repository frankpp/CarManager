#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QApplication

from mainWindow import MainWindow

app = QApplication(sys.argv)
window = MainWindow()
#window = ToFuelRecordForm()

window.show()
sys.exit(app.exec_())
